package com.mars.ecsheet.service;


import com.alibaba.fastjson.JSONObject;

/**
 * @author Eric
 * @date 2024/07/31
 * @description
 */
public interface IMessageProcess {


    /**
     * 对updateurl发来的信息进行处理
     * @param message
     * @return
     */
    void process(String gridKey, JSONObject message);
}
