package com.mars.ecsheet;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author Eric
 * @date 2024/07/31
 * @description
 */
@SpringBootApplication
public class EcsheetApplication {
    public static void main(String[] args) {
        SpringApplication.run(EcsheetApplication.class, args);
    }

}
